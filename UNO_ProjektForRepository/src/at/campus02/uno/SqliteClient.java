package at.campus02.uno;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class SqliteClient {
	private Connection connection = null;

	public SqliteClient(String UNODatenbank) throws SQLException {
		connection = DriverManager.getConnection("jdbc:sqlite:" + UNODatenbank);
	}

	public boolean tableExists(String UNOSpiel) throws SQLException {
		String query = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + UNOSpiel + "';";
		return executeQuery(query).size() > 0;
	}

	public void executeStatement(String sqlStatement) throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
		statement.executeUpdate(sqlStatement);
	}

	public ArrayList<HashMap<String, String>> executeQuery(String sqlQuery) throws SQLException {
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30); // set timeout to 30 sec.
		ResultSet rs = statement.executeQuery(sqlQuery);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		while (rs.next()) {
			HashMap<String, String> map = new HashMap<String, String>();
			for (int i = 1; i <= columns; i++) {
				String value = rs.getString(i);
				String key = rsmd.getColumnName(i);
				map.put(key, value);
			}
			result.add(map);
		}
		return result;
	}

	public void init() {

		try {
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		try {

			// 2. get Connection to database
			connection = DriverManager.getConnection(
					"jdbc:ucanaccess://" + "E:\\Software Entwicklung\\Projekte\\SQLiteDatabaseBrowserPortable\\"
							+ "SQLiteDatabaseBrowserPortable.exe");
			System.out.println("Super - Verbindung wurde aufgebaut");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<DBSpiel> StandRunde() {

		ArrayList<DBSpiel> spiel = new ArrayList<>();
		try {
			String selectMessengerSQLight = "";
			selectMessengerSQLight += "SELECT *";
			selectMessengerSQLight += " FROM Spiel";

			PreparedStatement stmt = connection.prepareStatement(selectMessengerSQLight);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				DBSpiel s;

				s = new DBSpiel(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));

				spiel.add(s);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return spiel;

	}
}