package at.campus02.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel {

	private ArrayList<UnoKarte> kartenstapel = new ArrayList<UnoKarte>();
	private ArrayList<UnoKarte> ablage = new ArrayList<UnoKarte>();
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();
	private Spieler aktuellerSpieler = null;
	private int nummerSpiel;
	private int nummerRunde;
	private String separator = "--------------------------------------------------------------------";

	public UnoSpiel() {
		for (int counter = 0; counter < 2; counter++) {
			kartenstapel.add(new UnoKarte("Rot", "Null", 0));
			kartenstapel.add(new UnoKarte("Rot", "Eins", 1));
			kartenstapel.add(new UnoKarte("Rot", "Zwei", 2));
			kartenstapel.add(new UnoKarte("Rot", "Drei", 3));
			kartenstapel.add(new UnoKarte("Rot", "Vier", 4));
			kartenstapel.add(new UnoKarte("Rot", "Fuenf", 5));
			kartenstapel.add(new UnoKarte("Rot", "Sechs", 6));
			kartenstapel.add(new UnoKarte("Rot", "Sieben", 7));
			kartenstapel.add(new UnoKarte("Rot", "Acht", 8));
			kartenstapel.add(new UnoKarte("Rot", "Neun", 9));
			kartenstapel.add(new UnoKarte("Rot", "Sperre", 20));
			kartenstapel.add(new UnoKarte("Rot", "NimmZwei", 20));
			kartenstapel.add(new UnoKarte("Rot", "Richtungswechsel", 20));
			kartenstapel.add(new UnoKarte("Blau", "Null", 0));
			kartenstapel.add(new UnoKarte("Blau", "Eins", 1));
			kartenstapel.add(new UnoKarte("Blau", "Zwei", 2));
			kartenstapel.add(new UnoKarte("Blau", "Drei", 3));
			kartenstapel.add(new UnoKarte("Blau", "Vier", 4));
			kartenstapel.add(new UnoKarte("Blau", "Fuenf", 5));
			kartenstapel.add(new UnoKarte("Blau", "Sechs", 6));
			kartenstapel.add(new UnoKarte("Blau", "Sieben", 7));
			kartenstapel.add(new UnoKarte("Blau", "Acht", 8));
			kartenstapel.add(new UnoKarte("Blau", "Neun", 9));
			kartenstapel.add(new UnoKarte("Blau", "Sperre", 20));
			kartenstapel.add(new UnoKarte("Blau", "NimmZwei", 20));
			kartenstapel.add(new UnoKarte("Blau", "Richtungswechsel", 20));
			kartenstapel.add(new UnoKarte("Gelb", "Null", 0));
			kartenstapel.add(new UnoKarte("Gelb", "Eins", 1));
			kartenstapel.add(new UnoKarte("Gelb", "Zwei", 2));
			kartenstapel.add(new UnoKarte("Gelb", "Drei", 3));
			kartenstapel.add(new UnoKarte("Gelb", "Vier", 4));
			kartenstapel.add(new UnoKarte("Gelb", "Fuenf", 5));
			kartenstapel.add(new UnoKarte("Gelb", "Sechs", 6));
			kartenstapel.add(new UnoKarte("Gelb", "Sieben", 7));
			kartenstapel.add(new UnoKarte("Gelb", "Acht", 8));
			kartenstapel.add(new UnoKarte("Gelb", "Neun", 9));
			kartenstapel.add(new UnoKarte("Gelb", "Sperre", 20));
			kartenstapel.add(new UnoKarte("Gelb", "NimmZwei", 20));
			kartenstapel.add(new UnoKarte("Gelb", "Richtungswechsel", 20));
			kartenstapel.add(new UnoKarte("Gruen", "Null", 0));
			kartenstapel.add(new UnoKarte("Gruen", "Eins", 1));
			kartenstapel.add(new UnoKarte("Gruen", "Zwei", 2));
			kartenstapel.add(new UnoKarte("Gruen", "Drei", 3));
			kartenstapel.add(new UnoKarte("Gruen", "Vier", 4));
			kartenstapel.add(new UnoKarte("Gruen", "Fuenf", 5));
			kartenstapel.add(new UnoKarte("Gruen", "Sechs", 6));
			kartenstapel.add(new UnoKarte("Gruen", "Sieben", 7));
			kartenstapel.add(new UnoKarte("Gruen", "Acht", 8));
			kartenstapel.add(new UnoKarte("Gruen", "Neun", 9));
			kartenstapel.add(new UnoKarte("Gruen", "Sperre", 20));
			kartenstapel.add(new UnoKarte("Gruen", "NimmZwei", 20));
			kartenstapel.add(new UnoKarte("Gruen", "Richtungswechsel", 20));
			kartenstapel.add(new UnoKarte("Schwarz", "WuenschFarbe", 50));
			kartenstapel.add(new UnoKarte("Schwarz", "WuenschFarbe", 50));
			kartenstapel.add(new UnoKarte("Schwarz", "WuenschFarbePlusVier", 50));
			kartenstapel.add(new UnoKarte("Schwarz", "WuenschFarbePlusVier", 50));
		}

		mischen();
	}

	public void run() {
		// Initialisierung

		austeilen();
		Collections.shuffle(mitspieler);

		System.out.println();
		System.out.println("Es spielen folgende Spieler:");
		int i = 1;

		for (Spieler spieler : mitspieler) {
			System.out.println("Spieler " + i + ": " + spieler.getName());
			i++;
		}

		System.out.println();
		System.out.println(separator);
		System.out.println("Jetzt geht's los!");
		System.out.println("Die neue Rundennummer ist " + getNummerRunde());
		System.out.println(separator);
		System.out.println();
		System.out.println(separator);
		System.out.println("Oben liegt: " + ablage.get(0));
		System.out.println(separator);
		// Update
		try {
			while (spielzug()) {

			}
		} catch (CardLostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// exit
		System.out.println(separator);
		if (aktuellerSpieler.getIstMensch()) {
			System.out
					.println(aktuellerSpieler.getName() + " hat gewonnen!!! Gratulation!!! Smilies und Feuerwerke!!!");
		} else {
			System.out.println(aktuellerSpieler.getName() + " hat gewonnen!");
			mitspieler.add(aktuellerSpieler);
		}

		System.out.println(separator);

	}

	public ArrayList<Spieler> getMitspieler() {
		return mitspieler;
	}

	public int getNummerSpiel() {
		return nummerSpiel;
	}

	public int getNummerRunde() {
		return nummerRunde;
	}

	public void mischen() {
		for (UnoKarte k : ablage) {

			if (k.getWert() == 50) {
				k.setFarbe("Schwarz");
			}
		}
		Collections.shuffle(kartenstapel);
	}

	public void mitspielen(Spieler neuerSpieler) {
		mitspieler.add(neuerSpieler);
	}

	public void austeilen() {
		for (int counter = 0; counter < 7; counter++) {
			for (Spieler spieler : mitspieler) {
				UnoKarte abgehobeneKarte = kartenstapel.remove(0);
				spieler.aufnehmen(abgehobeneKarte);
			}
		}

		while (true) {

			UnoKarte aufgedeckteKarte;
			aufgedeckteKarte = kartenstapel.remove(0);
			ablage.add(0, aufgedeckteKarte);

			if (aufgedeckteKarte.getWert() <= 9)
				break;
		}

	}

	public boolean spielzug() throws CardLostException {
		aktuellerSpieler = mitspieler.remove(0); // jetzt wird gespielt
		Spieler naechsterSpieler = null;
		UnoKarte karte = null;
		if (aktuellerSpieler.getIstMensch()) {
			System.out.println();
			System.out.println(aktuellerSpieler.getName() + " ist dran.");
			System.out.println(separator);
			System.out.println("Oben liegt " + ablage.get(0));
			System.out.println(separator);
			System.out.printf(aktuellerSpieler.getName() + " hat %d Handkarten.%n",
					aktuellerSpieler.getHandkarten().size());
			System.out.println();
			System.out.println(aktuellerSpieler.getName() + "s Handkarten sind:");
			System.out.println(aktuellerSpieler.handKartenHashmap().toString());
			System.out.println(separator);
			System.out.println();

		} else {
			System.out.printf("%s ist dran.\n", aktuellerSpieler.getName());
		}

		UnoKarte obersteKarte = ablage.get(0); // bleibt am Stapel

		if (aktuellerSpieler.getIstMensch()) {
			while (aktuellerSpieler.isfalscheKarte()) {
				karte = aktuellerSpieler.ausw�hlen(aktuellerSpieler.handleInputKartenAuswahl(), obersteKarte);
			}
			aktuellerSpieler.setFalscheKarte(true);
		} else
			karte = aktuellerSpieler.ausw�hlen(aktuellerSpieler.handleInputKartenAuswahl(), obersteKarte);

		if (aktuellerSpieler.isStrafkarten() == true) {
			UnoKarte abgehobeneKarte = kartenstapel.remove(0);
			aktuellerSpieler.aufnehmen(abgehobeneKarte);
			UnoKarte abgehobeneKarte1 = kartenstapel.remove(0);
			aktuellerSpieler.aufnehmen(abgehobeneKarte1);
			System.out.println("Uno sagen vergessen oder Uno zum falschen Zeitpunkt gesagt, zwei Strafkarten ziehen.");
			aktuellerSpieler.setStrafkarten(false);
		} // nach falscher Uno meldung steht nur muss Karte aufnehmen nicht muss zwei
			// karten aufnehmen

		if (karte != null) // eine passende Karte
		{

			ablage.add(0, karte); // an die 1ste stelle einfuegen

			System.out.printf("%s legt %s ab \n", aktuellerSpieler.getName(), karte);

			switch (karte.getZahl()) {
			case "Sperre":

				if (karte.getZahl() == "Sperre") {
					naechsterSpieler = mitspieler.remove(0);
					System.out.printf("Sperre: %s muss aussetzen \n", naechsterSpieler.getName());

					break;
				}
			case "NimmZwei":
				if (karte.getZahl() == "NimmZwei") {

					System.out.printf("NimmZwei: %s muss zwei Karten ziehen \n", mitspieler.get(0).getName());

					if (kartenstapel.size() < 2) {
						System.out.println("...Umsortieren...");
						UnoKarte obersteKarte1 = ablage.remove(0);
						kartenstapel.addAll(ablage); // Alle Eintraege einfuegen
						mischen();

						ablage.clear(); // ablageStapel leeren
						ablage.add(obersteKarte1); // alte oberste Karte einfuegen
					}

					UnoKarte abgehobeneKarte = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte);
					UnoKarte abgehobeneKarte1 = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte1);

					naechsterSpieler = mitspieler.remove(0);

					break;
				}

				break;
			case "Richtungswechsel":

				if (karte.getZahl() == "Richtungswechsel") {
					java.util.Collections.reverse(mitspieler);
					System.out.printf("Richtungswechsel: %s is in der Reihe \n", mitspieler.get(0).getName());

					break;
				}

				break;
			case "WuenschFarbe":

				if (karte.getZahl() == "WuenschFarbe") {

					karte.setFarbe(aktuellerSpieler.handleInputFarbAuswahl());

					if (karte.getFarbe() == null) {
						System.out.println(
								"Bei der Farbwahl ist leider etwas schief gelaufen, die Farbe wird automatisch auf Rot eingestellt.");
						karte.setFarbe("Rot");
					}

					break;
				}

				break;
			case "WuenschFarbePlusVier":

				if (karte.getZahl() == "WuenschFarbePlusVier") {

					karte.setFarbe(aktuellerSpieler.handleInputFarbAuswahl());

					if (karte.getFarbe() == null) {
						System.out.println(
								"Bei der Farbwahl ist leider etwas schief gelaufen, die Farbe wird automatisch auf Rot eingestellt.");
						karte.setFarbe("Rot");
					}

					// karten ziehen + umsortieren gemeinsam in eine methode umprogrammieren!!!
					if (kartenstapel.size() < 4) {
						System.out.println("...Umsortieren...");
						UnoKarte obersteKarte1 = ablage.remove(0);
						kartenstapel.addAll(ablage); // Alle Eintraege einfuegen
						mischen();

						ablage.clear(); // ablageStapel leeren
						ablage.add(obersteKarte1); // alte oberste Karte einfuegen

					}

					System.out.printf("%s muss vier Karten ziehen \n", mitspieler.get(0).getName());
					UnoKarte abgehobeneKarte = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte);
					UnoKarte abgehobeneKarte1 = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte1);
					UnoKarte abgehobeneKarte2 = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte2);
					UnoKarte abgehobeneKarte3 = kartenstapel.remove(0);
					mitspieler.get(0).aufnehmen(abgehobeneKarte3);

					naechsterSpieler = mitspieler.remove(0);

					break;
				}

			}

		} else

		{
			UnoKarte abgehobeneKarte = abheben();
			aktuellerSpieler.aufnehmen(abgehobeneKarte);
			System.out.printf("%s muss Karte aufnehmen \n", aktuellerSpieler.getName());
		}

		if (!aktuellerSpieler.isFertig())
			mitspieler.add(aktuellerSpieler);

		if (naechsterSpieler != null)
			mitspieler.add(naechsterSpieler);

		int sum = ablage.size() + kartenstapel.size();
		for (Spieler s : mitspieler) {
			sum += s.getHandkarten().size();

		}
		if (sum != 112) { // handkarten noch irgendwie dazu mit variable
			throw new CardLostException(String.format("es sind %d Karten im Spiel, 112 wurden erwartet \n", sum),
					String.format("Ablage: %d \n Kartenstapel: %d \n", ablage.size(), kartenstapel.size()));

		}
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return mitspieler.size() > 3;

	}

	private UnoKarte abheben() {

		if (kartenstapel.isEmpty()) {
			System.out.println("...Umsortieren...");
			UnoKarte obersteKarte = ablage.remove(0);
			kartenstapel.addAll(ablage); // Alle Eintraege einfuegen
			mischen();

			ablage.clear(); // ablageStapel leeren
			ablage.add(obersteKarte); // alte oberste Karte einfuegen

			return kartenstapel.remove(0);

		} else {

			UnoKarte abgehobeneKarte = kartenstapel.remove(0);
			return abgehobeneKarte;
		}
	}

	public ArrayList<UnoKarte> getKartenStapel() {
		return kartenstapel;
	}

	public String toString() {
		if (ablage.isEmpty()) {
			return String.format("%s am Stapel sind %d Karten", mitspieler, kartenstapel.size());
		}
		return String.format("%s am Stapel sind %d Karten %s", mitspieler, kartenstapel.size(), ablage.get(0));
	}

	public void clearAblage() {
		kartenstapel.addAll(ablage);
		ablage.clear();
	}

	public void clearHandkarten() {
		for (Spieler s : mitspieler) {
			kartenstapel.addAll(s.getHandkarten());
			s.getHandkarten().clear();
		}
	}

	public void nextSpielNummer() {
		this.nummerSpiel += 1;
	}

	public void nextRundenNummer() {
		this.nummerRunde += 1;
	}

	public void setSpielnummer(int spielNummer) {
		this.nummerSpiel = spielNummer;

	}

	public void setRundenNummer(int rundenNummer) {
		this.nummerRunde = rundenNummer;
	}

	public Spieler getAktuellerSpieler() {
		return aktuellerSpieler;
	}

}