package at.campus02.uno;

public class UnoKarte {

	private String farbe;
	private String zahl;
	private int wert;

	public UnoKarte(String farbe, String zahl, int wert) {
		this.farbe = farbe;
		this.zahl = zahl;
		this.wert = wert;
	}

	public String getZahl() {
		return zahl;
	}

	public String getFarbe() {
		return farbe;
	}

	public String getFarbe(String Farbe) {
		return Farbe;
	}

	public int getWert() {
		return wert;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public boolean passtDrauf(UnoKarte andereKarte) {

		if (this.farbe == andereKarte.getFarbe())
			return true;

		if (this.zahl == "Null")
			if (andereKarte.getZahl() == "Null")
				return true;

		if (this.zahl == "Eins")
			if (andereKarte.getZahl() == "Eins")
				return true;

		if (this.zahl == "Zwei")
			if (andereKarte.getZahl() == "Zwei")
				return true;
		if (this.zahl == "Drei")
			if (andereKarte.getZahl() == "Drei")
				return true;

		if (this.zahl == "Vier")
			if (andereKarte.getZahl() == "Vier")
				return true;

		if (this.zahl == "Fuenf")
			if (andereKarte.getZahl() == "Fuenf")
				return true;

		if (this.zahl == "Sechs")
			if (andereKarte.getZahl() == "Sechs")
				return true;

		if (this.zahl == "Sieben")
			if (andereKarte.getZahl() == "Sieben")
				return true;

		if (this.zahl == "Acht")
			if (andereKarte.getZahl() == "Acht")
				return true;

		if (this.zahl == "Neun")
			if (andereKarte.getZahl() == "Neun")
				return true;

		if (this.zahl == "Sperre")
			if (andereKarte.getZahl() == "Sperre")
				return true;

		if (this.zahl == "NimmZwei")
			if (andereKarte.getZahl() == "NimmZwei")
				return true;

		if (this.zahl == "Richtungswechsel")
			if (andereKarte.getZahl() == "Richtungswechsel")
				return true;

		if (this.wert == 50)
			return true;

		return false;
	}

	public String toString() {
		return String.format("(%s,%s)", farbe, zahl);
	}

}
