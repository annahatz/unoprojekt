package at.campus02.uno;

public class DBSpiel {
	private String Spieler;
	private int Spiel;
	private int Runde;
	private int GesamtPunkte;

	public DBSpiel(String string, int int1, int int2, int int3) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the spieler
	 */
	public String getSpieler() {
		return Spieler;
	}

	/**
	 * @param spieler the spieler to set
	 */
	public void setSpieler(String spieler) {
		Spieler = spieler;
	}

	/**
	 * @return the spiel
	 */
	public int getSpiel() {
		return Spiel;
	}

	/**
	 * @param spiel the spiel to set
	 */
	public void setSpiel(int spiel) {
		Spiel = spiel;
	}

	/**
	 * @return the runde
	 */
	public int getRunde() {
		return Runde;
	}

	/**
	 * @param runde the runde to set
	 */
	public void setRunde(int runde) {
		Runde = runde;
	}

	/**
	 * @return the gesamtPunkte
	 */
	public int getGesamtPunkte() {
		return GesamtPunkte;
	}

	/**
	 * @param gesamtPunkte the gesamtPunkte to set
	 */
	public void setGesamtPunkte(int gesamtPunkte) {
		GesamtPunkte = gesamtPunkte;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DBSpiel [Spieler=" + Spieler + ", Spiel=" + Spiel + ", Runde=" + Runde + ", GesamtPunkte="
				+ GesamtPunkte + "]";
	}

}
