package at.campus02.uno;

public class CardLostException extends Exception {

	String details;

	public CardLostException(String format, String details) {
		super(format);
		this.details = details;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CardLostException [details=" + details + "]";
	}

}
