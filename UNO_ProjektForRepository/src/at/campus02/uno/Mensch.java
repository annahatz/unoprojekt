package at.campus02.uno;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Mensch extends Spieler {

	int inputCard;
	boolean abheben = false;
	boolean strafkarten = false;
	boolean falscheKarte = true;

	private Scanner scanner = new Scanner(System.in);

	public Mensch(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public void aufnehmen(UnoKarte neueKarte) {
		super.aufnehmen(neueKarte);
	}

	public boolean isFertig() {
		return super.isFertig();
	}

	public String getName() {
		return super.getName();
	}

	public ArrayList<UnoKarte> getHandkarten() {
		return super.getHandkarten();
	}

	public String toString() {
		return super.toString();
	}

	public int handkartenWert() {
		return super.handkartenWert();
	}

	public HashMap<Integer, UnoKarte> handKartenHashmap() {
		HashMap<Integer, UnoKarte> handkartenHashmap = new HashMap<Integer, UnoKarte>();

		for (int i = 0; i < this.getHandkarten().size(); i++) {
			handkartenHashmap.put(i, this.getHandkarten().get(i));
		}

		return handkartenHashmap;
	}

	public UnoKarte ausw�hlen(int i, UnoKarte obersteKarte) {

		UnoKarte zuVergleichendeKarte;

		if (abheben) {
			zuVergleichendeKarte = null;
			abheben = false;
			falscheKarte = false;
		} else {

			zuVergleichendeKarte = getHandkarten().get(i);

			if (zuVergleichendeKarte.passtDrauf(obersteKarte)) {
				this.getHandkarten().remove(i);
				falscheKarte = false;

			} else {
				System.out.println("Diese Karte passt nicht, bitte w�hlen Sie eine andere.");
				falscheKarte = true;
			}

		}
		return zuVergleichendeKarte;
	}

	@Override
	public boolean getIstMensch() {
		return true;
	}

	public boolean isStrafkarten() {
		return strafkarten;
	}

	public boolean setStrafkarten(boolean strafkarten) {
		return this.strafkarten = strafkarten;
	}

	@Override
	public boolean isfalscheKarte() {
		return falscheKarte;
	}

	@Override
	public boolean setFalscheKarte(boolean falscheKarte) {
		// TODO Auto-generated method stub
		return this.falscheKarte = falscheKarte;
	}

	public int handleInputKartenAuswahl() {

		System.out.println("Bitte w�hlen Sie eine Karte aus oder dr�cken Sie h um eine Karte zu heben.");
		System.out.println("Vergessen Sie nicht UNO zu sagen, BEVOR Sie die vorletzte Karte ablegen.");
		String input = scanner.nextLine().toLowerCase();
		boolean unoGesagt = false;
		boolean zweiHandkarten = false;

		// UNO sagen Handling Anfang
		if (input.equals("uno")) {
			unoGesagt = true;
			System.out.println("UNO gesagt, Karte ausw�hlen");
			input = scanner.nextLine().toLowerCase();
		}

		if (getHandkarten().size() == 2) {
			zweiHandkarten = true;
		}

		if (unoGesagt != zweiHandkarten) {
			strafkarten = true;
		}
		// UNO sagen Handling Ende

		if (input.equals("h")) {
			abheben = true;
			strafkarten = false;
			return 0;

		} else
			try {
				if (Integer.parseInt(input) >= 0 && Integer.parseInt(input) <= (getHandkarten().size() - 1))
					inputCard = Integer.parseInt(input);
				return inputCard;
			}

			catch (Exception e) {
				System.out.println("Diese Eingabe ist nicht erlaubt.");
				handleInputKartenAuswahl();
			}

		return inputCard;
		// Wird eine zu gro�e Ziffer eingegebn kommt nur Karte passt nicht und nicht
		// ung�ltige Eingabe

	}

	@Override
	public String handleInputFarbAuswahl() {
		String farbe = null;
		boolean gueltigeEingabe;
		System.out.println("Bitte w�hlen Sie eine Farbe aus.");
		System.out.println("Dr�cken Sie 1 f�r Blau.");
		System.out.println("Dr�cken Sie 2 f�r Rot.");
		System.out.println("Dr�cken Sie 3 f�r Gr�n.");
		System.out.println("Dr�cken Sie 4 f�r Gelb.");

		String farbAuswahlString = scanner.nextLine();
		int farbAuswahl = 0;

		try {
			farbAuswahl = Integer.parseInt(farbAuswahlString);
			gueltigeEingabe = true;
		} catch (Exception e) {

			gueltigeEingabe = false;
		}
		if (gueltigeEingabe) {
			switch (farbAuswahl) {
			case 1:
				farbe = "Blau";
				System.out.println("Sie haben Blau gew�hlt.");
				break;
			case 2:
				farbe = "Rot";
				System.out.println("Sie haben Rot gew�hlt.");
				break;
			case 3:
				farbe = "Gruen";
				System.out.println("Sie haben Gr�n gew�hlt.");
				break;
			case 4:
				farbe = "Gelb";
				System.out.println("Sie haben Gelb gew�hlt.");
				break;
			default:
				System.out.println("Ung�ltige Eingabe.");
				handleInputFarbAuswahl();
			}
			return farbe;

		} else {
			System.out.println("Ung�ltige Eingabe.");
			handleInputFarbAuswahl();
		}

		return null;
	}

}
