package at.campus02.uno;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Bot extends Spieler {

	public Bot(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public void aufnehmen(UnoKarte neueKarte) {
		super.aufnehmen(neueKarte);
	}

	public String getName() {
		return super.getName();
	}

	public boolean isFertig() {
		return super.isFertig();
	}

	public ArrayList<UnoKarte> getHandkarten() {

		return super.getHandkarten();
	}

	public String toString() {
		return super.toString();
	}

	public int handkartenWert() {
		return super.handkartenWert();
	}

	public UnoKarte ausw�hlen(int i, UnoKarte obersteKarte) {
		for (UnoKarte vergleichsKarte : this.getHandkarten()) {
			if (vergleichsKarte.passtDrauf(obersteKarte)) {
				this.getHandkarten().remove(vergleichsKarte);
				if (this.getHandkarten().size() == 1)
					System.out.printf("%s UNO! \n", this.getName());

				if (isFertig())
					System.out.printf("%s UNO UNO!! \n", this.getName());// err = ausgabe konsole in rot
				return vergleichsKarte;
			}

		}

		return null;
	}

	@Override
	public boolean getIstMensch() {
		return false;
	}

	@Override
	public HashMap<Integer, UnoKarte> handKartenHashmap() {
		// TODO Auto-generated method stub
		return null;
	}

	public int handleInputKartenAuswahl() {
		return 0;
	}

	@Override
	public String handleInputFarbAuswahl() {
//		TODO eventuell schauen dass der Bot nicht die Farbe ausw�hlt, die vor der Farbwahl war.
		ArrayList<String> farben = new ArrayList<String>();

		farben.add("Rot");
		farben.add("Blau");
		farben.add("Gruen");
		farben.add("Gelb");

		Collections.shuffle(farben);

		String farbe = farben.get(0);
		System.out.println(this.getName() + " w�nscht sich " + farbe);
		return farbe;
	}

	@Override
	public boolean isStrafkarten() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setStrafkarten(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isfalscheKarte() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setFalscheKarte(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

}
