package at.campus02.uno;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Spieler {

	private String name;
	private ArrayList<UnoKarte> handkarten;

	public Spieler(String name) {
		this.name = name;

		handkarten = new ArrayList<UnoKarte>();
	}

	public void aufnehmen(UnoKarte neueKarte) {
		handkarten.add(neueKarte);
	}

	public boolean isFertig() {
		return handkarten.isEmpty();
	}

	public String getName() {
		return name;
	}

	public abstract boolean getIstMensch();

	public abstract boolean isStrafkarten();

	public abstract boolean setStrafkarten(boolean b);

	public abstract boolean isfalscheKarte();

	public abstract boolean setFalscheKarte(boolean b);

	public abstract UnoKarte ausw�hlen(int i, UnoKarte obersteKarte);

	public ArrayList<UnoKarte> getHandkarten() {
		return handkarten;
	}

	public int handkartenWert() {
		int sum = 0;
		for (UnoKarte uk : handkarten) {
			sum += uk.getWert();
		}
		return sum;
	}

	public abstract HashMap<Integer, UnoKarte> handKartenHashmap();

	public abstract int handleInputKartenAuswahl();

	public abstract String handleInputFarbAuswahl();

	public String toString() {
		
		return String.format("%s hat %d Karten", name, handkarten.size());
	}

}