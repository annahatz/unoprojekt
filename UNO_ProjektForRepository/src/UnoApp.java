
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

import at.campus02.uno.CardLostException;
import at.campus02.uno.Bot;
import at.campus02.uno.Mensch;
import at.campus02.uno.Spieler;
import at.campus02.uno.SqliteClient;
import at.campus02.uno.UnoSpiel;

public class UnoApp {

	private static final String CREATETABLE = "CREATE TABLE Spiel (Spieler varchar(100) NOT NULL, Spiel int NOT NULL, Runde int NOT NULL, GesamtPunkte int NOT NULL, CONSTRAINT PK_Spiel PRIMARY KEY (Spieler, Spiel, Runde));";
	private static final String INSERT_TEMPLATE = "INSERT INTO Spiel (Spieler, Spiel, Runde, GesamtPunkte) VALUES ('%1s', %2d, %3d, %4d);";
	private static final String SELECT_BYPLAYERANDSESSION = "SELECT Spieler, SUM(GesamtPunkte) AS GesamtPunkte FROM Spiel WHERE Spieler = '%1s' AND Spiel = %2d;";
	private static final String SELECT_BYPLAYER = "SELECT SUM(GesamtPunkte) AS GesamtPunkte FROM Spiel WHERE Spieler = '%1s';";
	private static final String SELECT_ALL_SITZUNG = "SELECT * FROM Spiel WHERE Spiel = '%1d';";
	private static final String SELECT_ALL = "SELECT * FROM Spiel;";
	private static final String UPDATE_TEMPLATE = "UPDATE Spiel SET GesamtPunkte = %1d WHERE Spieler = '%2s' AND Spiel = '%3d';";

	public static void main(String[] args) throws CardLostException {

		ArrayList<Spieler> spieler = new ArrayList<Spieler>();
		UnoSpiel uno = new UnoSpiel();
		boolean ueber500 = false;
		System.out.println("Willkommen bei Uno!");
		setupPlayers(spieler, uno);
		try {
			SqliteClient client = new SqliteClient("demo.sqlite");
			setupGame(uno, client);
			int turnNumber = 0;
			while (!ueber500) {
				setupRound(uno, client, turnNumber);
				uno.run();
				showStatistics(uno, client);
				clearBoard(uno);
				ueber500 = hasPlayerLost(uno, client);
				turnNumber++;
			}
			handleGameEnd(client);
		} catch (SQLException ex) {
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}

	}

	private static void clearBoard(UnoSpiel uno) {
		uno.clearAblage();
		uno.clearHandkarten();
	}

	private static void setupPlayers(ArrayList<Spieler> spieler, UnoSpiel uno) {
		int j = getAnzahlMenschen();

		addBots(spieler);
		addPlayers(spieler, j);

		Collections.reverse(spieler);

		for (int i = 0; i < 4; i++) {
			uno.mitspielen(spieler.get(i));
		}
	}

	private static void setupGame(UnoSpiel uno, SqliteClient client) throws SQLException {
		if (!client.tableExists("Spiel")) {
			client.executeStatement(CREATETABLE);
		}

		int spielNummer = 0;

		String maxGameNo = client.executeQuery("SELECT Max(Spiel) AS Spiel FROM Spiel").get(0).get("Spiel");
		if (maxGameNo != null) {
			spielNummer = Integer.parseInt(maxGameNo) + 1;
		}
		uno.setSpielnummer(spielNummer);
		System.out.println("Die Spielnummer ist " + uno.getNummerSpiel());
	}

	private static void handleGameEnd(SqliteClient client) throws SQLException {
		System.out.println();
		System.out.println("Ein Spieler hat �ber 500 Punkte erreicht und damit das Spiel verloren.");
		System.out.println();
		System.out.println("Wollen Sie die gesamte Historie einsehen? j f�r ja, beliebige Taste f�r nein");
		Scanner abrufHistorieScanner = new Scanner(System.in);
		String abrufHistorie = abrufHistorieScanner.nextLine();
		if (abrufHistorie.equals("j")) {
			printHistorie(client);
		}
		System.out.println();
		System.out.println("Wollen Sie die Gesamtpunkte eines Spielers einsehen? j f�r ja, beliebige Taste f�r nein");
		Scanner abrufPunkteSpielerScanner = new Scanner(System.in);
		String abrufPunkteSpieler = abrufPunkteSpielerScanner.nextLine();
		System.out.println();
		if (abrufPunkteSpieler.equals("j")) {
			System.out.println("Bitte geben Sie den Namen des Spielers ein.");
			Scanner nameSpielerScanner = new Scanner(System.in);
			String nameSpieler = nameSpielerScanner.nextLine();
			printPunkteSpieler(client, nameSpieler);
			System.out.println();
		}
		System.out.println("Danke f�r das Spiel.");
	}

	private static boolean hasPlayerLost(UnoSpiel uno, SqliteClient client) throws SQLException {
		boolean ueber500 = false;
		for (Spieler s1 : uno.getMitspieler()) {
			if (zaehlePunkte(uno, s1, client) == true) {
				ueber500 = true;
			}
		}
		return ueber500;
	}

	private static void showStatistics(UnoSpiel uno, SqliteClient client) throws SQLException {
		updateScore(uno, client);
		System.out.println("Die Punkte f�r diese Runde wurden der Datenbank hinzugef�gt");

		System.out.println("Wollen Sie die Daten dieser Runde aufrufen? j f�r ja, beliebige Taste f�r nein");

		Scanner abrufRundeScanner = new Scanner(System.in);
		String abrufRunde = abrufRundeScanner.nextLine();
		if (abrufRunde.equals("j")) {
			printSitzung(uno, client);
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void setupRound(UnoSpiel uno, SqliteClient client, int i) throws SQLException {
		uno.setRundenNummer(i);
		addPlayersToDB(uno, client);
	}

	private static void addPlayersToDB(UnoSpiel uno, SqliteClient client) throws SQLException {
		for (Spieler s : uno.getMitspieler()) {

			client.executeStatement(
					String.format(INSERT_TEMPLATE, s.getName(), uno.getNummerSpiel(), uno.getNummerRunde(), 0));
		}
	}

	private static void addPlayers(ArrayList<Spieler> spieler, int j) {
		for (int i = 0; i < j; i++) {
			if (!addPlayer(spieler)) {
				i--;
			}
		}
	}

	private static boolean addPlayer(ArrayList<Spieler> spieler) {
		System.out.println("Bitte geben Sie Ihren Namen ein:");
		Scanner inputName = new Scanner(System.in);
		String nameMensch = inputName.nextLine();
		if (isNameUnique(spieler, nameMensch)) {

			Mensch mensch = new Mensch(nameMensch);
			spieler.add(mensch);
		} else {
			System.out.println("Diesen Namen gibt es schon im Spiel, bitte w�hlen Sie einen anderen.");
			return false;
		}
		return true;
	}

	private static void addBots(ArrayList<Spieler> spieler) {
		spieler.add(new Bot("Botbert"));
		spieler.add(new Bot("Bottraud"));
		spieler.add(new Bot("Bothilde"));
		spieler.add(new Bot("Botfred"));
	}

	private static boolean isNameUnique(ArrayList<Spieler> spieler, String nameMensch) {
		for (Spieler s : spieler) {
			if (nameMensch.equals(s.getName())) {
				return false;
			}
		}
		return true;
	}

	private static int getAnzahlMenschen() {
//		TODO sicher gegen�ber falschen eingaben machen
		// angeblich mit do while

		System.out.println("Wie viele Menschen spielen mit?");
		Scanner inputAnzahlMenschen = new Scanner(System.in);
		String inputMenschen = inputAnzahlMenschen.nextLine();
		int anzahlMenschen = 0;
		try {
			anzahlMenschen = Integer.parseInt(inputMenschen);
			if (anzahlMenschen >= 0 && anzahlMenschen < 5) {
				return anzahlMenschen;
			} else {
				System.out.println("Ung�ltige Zahl, minimal 0, maximal 4 menschlich Mitspieler.");
				return getAnzahlMenschen();
			}

		} catch (NumberFormatException e) {
			System.out.println("Ung�ltige Eingabe.");
			return getAnzahlMenschen();
		}

	}

	private static void updateScore(UnoSpiel spiel, SqliteClient client) throws SQLException {
		for (Spieler s : spiel.getMitspieler()) {

			ArrayList<HashMap<String, String>> result = client
					.executeQuery(String.format(SELECT_BYPLAYERANDSESSION, s.getName(), spiel.getNummerSpiel()));

			int gesamt = Integer.parseInt(result.get(0).get("GesamtPunkte")) + s.handkartenWert();

			if (gesamt < 0) {
				gesamt = 0;
			}
			client.executeStatement(
					String.format(UPDATE_TEMPLATE, (int) gesamt, (String) s.getName(), (int) spiel.getNummerSpiel()));

		}
	}

	private static boolean zaehlePunkte(UnoSpiel spiel, Spieler s, SqliteClient client) throws SQLException {

		ArrayList<HashMap<String, String>> result = client.executeQuery(
				String.format(SELECT_BYPLAYERANDSESSION, s.getName(), spiel.getNummerSpiel(), spiel.getNummerRunde()));
		// pr�fen auf null, wenn null dann false zur�ckgeben

		int gesamt = Integer.parseInt(result.get(0).get("GesamtPunkte"));

		if (gesamt < 0) {
			gesamt = 0;

		}
		return gesamt > 500;

	}

	private static void printSitzung(UnoSpiel spiel, SqliteClient client) throws SQLException {
		ArrayList<HashMap<String, String>> result = client
				.executeQuery(String.format(SELECT_ALL_SITZUNG, spiel.getNummerSpiel()));

		for (HashMap<String, String> hashMap : result) {
			System.out.printf("Spielzug %s - Spieler %s - Punkte %s\n", hashMap.get("Runde"), hashMap.get("Spieler"),
					hashMap.get("GesamtPunkte"));
		}
	}

	private static void printHistorie(SqliteClient client) throws SQLException {
		ArrayList<HashMap<String, String>> result = client.executeQuery(String.format(SELECT_ALL));

		for (HashMap<String, String> hashMap : result) {
			System.out.printf("Spiel %s - Spielzug %s - Spieler %s - Punkte %s\n", hashMap.get("Spiel"),
					hashMap.get("Runde"), hashMap.get("Spieler"), hashMap.get("GesamtPunkte"));
		}
	}

	private static void printPunkteSpieler(SqliteClient client, String name) {
		try {
			ArrayList<HashMap<String, String>> result = client.executeQuery(String.format(SELECT_BYPLAYER, name));
			for (HashMap<String, String> hashMap : result) {
				if (hashMap.get("GesamtPunkte") != null) {
					System.out.printf("%s hat zur Zeit %s Gesamtpunkte", name, hashMap.get("GesamtPunkte"));
				} else {
					System.out.println("Dieser Spieler wurde nicht in der Datenbank gefunden.");
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
}